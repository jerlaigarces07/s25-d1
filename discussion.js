// Create fruits document

db.fruits.insertMany([
    {
      "name": "Apple",
      "color": "Red",
      "stock": 20,
      "price": 40,
      "supplier_id": 1,
      "onSale": true,
      "origin": ["Philippines", "US"]
    },
    {
      "name": "Banana",
      "color": "Yellow",
      "stock": 15,
      "price": 20,
      "supplier_id": 2,
      "onSale": true,
      "origin": ["Philippines", "Ecuador"]
    },
    {
      "name": "Kiwi",
      "color": "Green",
      "stock": 25,
      "price": 50,
      "supplier_id": 1,
      "onSale": true,
      "origin": ["US", "China"]
    },
    {
      "name": "Mango",
      "color": "Yellow",
      "stock": 10,
      "price": 120,
      "supplier_id": 2,
      "onSale": false,
      "origin": ["Philippines", "India"]
    }
]);

// MongoDB Aggregation
    // used to generate manipulated data and perform operations to create a filtered results that helps in analyzing data
    //  Using Aggregate method
    /* $match
        - used to pass documents that meet the specified condition(s) top the next pipeline stage/aggregation process.

        Syntax:
            { $match: {field: value} }

        $group
         - used to group elements together and field-value pairs using the data from the grouped elements
        Syntax:
            { $group: { _id: "value", fieldResult: "valueResult"} }
    */
        db.fruits.aggregate([
            { $match: { "onSale": true } },
            { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
        ]);

    // Sorting aggregated results
    /* $sort
        - can be used to change the order of the aggregated result
        Syntax:
            { $sort { field: 1/-1 } }
     */
        db.fruits.aggregate([
            { $match: { "onSale": true } },
            { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
            { $sort: { "total": 1 } }
        ]);

    // Aggregateing results based on array fields
    /* $unwind
        - deconstructs an array field from a collecton/field with an array value to output a result for each element
        Syntax: { $unwind: field }
     */
        db.fruits.aggregate([
            { $unwind: "$origin" }
        ]);

        db.fruits.aggregate([
            { $unwind: "$origin" },
            { $group: { _id: "$origin", kinds: { $sum: 1 } } }
        ]);
    
    // Schema Design
        // One-to-One Relationship

        var owner = ObjectId();

        db.owners.insert({
            _id: owner,
            name: "John Smith",
            contact: "09876542321"
        });

        db.suppliers.insert({
            name: "ABC Fruits",
            contact: "09998876543",
            owner_id: ObjectId("617770614beeefd7f0c9ae89")
        });

        // Update the owner document and insert new field
        db.owners.updateOne(
            { "_id": ObjectId("617770614beeefd7f0c9ae89") },
            { 
                $set: { 
                    "supplier_id": ObjectId("617771834beeefd7f0c9ae8a") 
                } 
            }
        );

        /* Result:
            _id: ObjectId("617770614beeefd7f0c9ae89"),,
            name: "John Smith",
            contact: "09876542321",
            supplier_id: ObjectId("617771834beeefd7f0c9ae8a")
        */

        // One-to-Many Relationship
         db.suppliers.insertMany([
             {
                "name": "DEF Fruits",
                "contact": "09632109847",
                "owner_id": ObjectId("617770614beeefd7f0c9ae89")
             },
             {
                "name": "GHI Fruits",
                "contact": "09065489712",
                "owner_id": ObjectId("617770614beeefd7f0c9ae89")
             }
            ]);

         db.owners.updateOne(
            { "_id": ObjectId("617770614beeefd7f0c9ae89") },
            { 
                $set: { 
                    "supplier_id": [ObjectId("617771834beeefd7f0c9ae8a"), ObjectId("61777a0b4beeefd7f0c9ae8b"), ObjectId("61777a0b4beeefd7f0c9ae8c")]
                } 
            }
        );

        // Many-to-Many Relationship 
        var owner = ObjectId();

        db.owners.insert({
            _id: owner,
            name: "Peter Shrew",
            contact: "09095623147",
            supplier_id: [ObjectId("617771834beeefd7f0c9ae8a"), ObjectId("61777a0b4beeefd7f0c9ae8b"), ObjectId("61777a0b4beeefd7f0c9ae8c")]
        });

        db.suppliers.updateOne(
            { "_id": ObjectId("617771834beeefd7f0c9ae8a") },
            {
                $set: {
                "owner_id": [ObjectId("617770614beeefd7f0c9ae89"), ObjectId("617790d84beeefd7f0c9ae8d")]
                }
            }
        );

         /* Result: db.suppliers.find()
            _id: ObjectId("617771834beeefd7f0c9ae8a"),
            name: "ABC Fruits",
            contact: "09998876543",
            owner_id: [ObjectId("617770614beeefd7f0c9ae89"), ]
        */